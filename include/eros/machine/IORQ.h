/* This file is generated. Do not edit. */

#ifndef __EROS_MACHINE_IORQ_H__
#define __EROS_MACHINE_IORQ_H__

#if defined(EROS_TARGET_i486)
#include "../arch/i486/IORQ.h"
#elif defined(EROS_TARGET_arm)
#include "../arch/arm/IORQ.h"
#else
#error "Unknown target"
#endif

#endif /* __EROS_MACHINE_IORQ_H__ */
