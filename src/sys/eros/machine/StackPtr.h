/* This file is generated. Do not edit. */

#ifndef __EROS_MACHINE_STACKPTR_H__
#define __EROS_MACHINE_STACKPTR_H__

#if defined(EROS_TARGET_i486)
#include "../arch/i486/StackPtr.h"
#elif defined(EROS_TARGET_arm)
#include "../arch/arm/StackPtr.h"
#else
#error "Unknown target"
#endif

#endif /* __EROS_MACHINE_STACKPTR_H__ */
